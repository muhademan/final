<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pegawai;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth')->only('create','store','edit','update','delete','show');
    }
    public function index()
    {
        //
        $tampil = Pegawai::all();
        return view('pages.pegawai',compact('tampil')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.create-pegawai');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'alamat' => 'required',
            'no_telpon' => 'required',
            'jenis_kelamin' => 'required',
            'pend_terakhir' => 'required',
        ]);

        $input = Pegawai::create([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'alamat' => $request['alamat'],
            'no_telpon' => $request['no_telpon'],
            'jenis_kelamin' => $request['jenis_kelamin'],
            'pend_terakhir' => $request['pend_terakhir'],
        ]);

        return redirect('/pegawai')->with('success','Berhasil Menambahkan Pegawai Baru!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tampil = Pegawai::find($id);
        return view('pages.show-pegawai',compact('tampil'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tampil = Pegawai::find($id);
        //
        return view('forms.edit-pegawai',compact('tampil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'alamat' => 'required',
            'no_telpon' => 'required',
            'jenis_kelamin' => 'required',
            'pend_terakhir' => 'required',
        ]);

        $update = Pegawai::where('id',$id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'alamat' => $request['alamat'],
            'no_telpon' => $request['no_telpon'],
            'jenis_kelamin' => $request['jenis_kelamin'],
            'pend_terakhir' => $request['pend_terakhir'],
        ]);

        return redirect('/pegawai')->with('success','Berhasil Merubah Data Pegawai!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Pegawai::destroy($id);
        return redirect('/pegawai')->with('success','Berhasil Menghapus Data Pegawai!!!');
    }
}
