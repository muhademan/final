<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;

class BarangController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->only('create','store','edit','update','delete','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tampil = Barang::all();

        return view('pages.dashboard',compact('tampil'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.create-barang');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama' => 'required',
            'stok' => 'required|numeric',
            'harga' => 'required|numeric',
            'tipe' => 'required',
        ]);

        $input = Barang::create([
            'nama' => $request['nama'],
            'stok' => $request['stok'],
            'harga' => $request['harga'],
            'tipe' => $request['tipe'],
        ]);

        return redirect('/dashboard')->with('success','Berhasil Menambahkan Data Barang!!!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tampil = Barang::find($id);

        return view('pages.show-barang',compact('tampil'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $edit = Barang::find($id);
        return view('forms.edit-barang',compact('edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nama' => 'required',
            'stok' => 'required|numeric',
            'harga' => 'required|numeric',
            'tipe' => 'required',
        ]);

        $update = Barang::where('id',$id)->update([
            'nama' => $request['nama'],
            'stok' => $request['stok'],
            'harga' => $request['harga'],
            'tipe' => $request['tipe'],
        ]);

        return redirect('/dashboard')->with('success','Berhasil Merubah Data Barang!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Barang::destroy($id);
        return redirect('/dashboard')->with('success','Berhasil Menghapus Data Barang!!!');
        //
    }
}
