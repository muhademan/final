<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penjualanbarang;

class PenjualanbarangController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->only('create', 'store', 'edit', 'update', 'delete', 'show');
    }
    public function index()
    {
        //
        $tampil = Penjualanbarang::all();
        return view('pages.penjualan', compact('tampil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('forms.create-penjualan');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama_barang' => 'required',
            'jumlah_yang_terjual' => 'required|numeric',
            'harga_yang_terjual' => 'required|numeric',
        ]);

        $input = Penjualanbarang::create([
            'nama_barang' => $request['nama_barang'],
            'jumlah_yang_terjual' => $request['jumlah_yang_terjual'],
            'harga_yang_terjual' => $request['harga_yang_terjual']
        ]);

        return redirect('/penjualan')->with('success', 'Berhasil Menambahkan Penjualan Barang!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tampil = Penjualanbarang::find($id);
        return view('pages.show-penjualan', compact('tampil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tampil = Penjualanbarang::find($id);
        //
        return view('forms.edit-penjualan', compact('tampil'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'nama_barang' => 'required',
            'jumlah_yang_terjual' => 'required|numeric',
            'harga_yang_terjual' => 'required|numeric',
        ]);

        $update = Penjualanbarang::where('id', $id)->update([
            'nama_barang' => $request['nama_barang'],
            'jumlah_yang_terjual' => $request['jumlah_yang_terjual'],
            'harga_yang_terjual' => $request['harga_yang_terjual']
        ]);

        return redirect('/penjualan')->with('success', 'Berhasil Merubah Data Penjualan Barang!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Penjualanbarang::destroy($id);
        return redirect('/penjualan')->with('success', 'Berhasil Menghapus Data Penjualan Barang!!!');
    }
}
