@extends('layouts.master')
@section('title','Data Barang di Gudang')
@section('content')

<div class="card card-primary mt-5">
    <div class="card-header">
        <h3 class="card-title"><small>Masukkan Barang yang ingin di inputkan</small></h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/barang-masuk" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama:</label>
                    <input type="text" class="form-control" id="nama" value="{{ old( 'nama','') }}" placeholder="Masukkan nama" name="nama">
                    @error('nama')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
            
            <div class="form-group">
                <label for="stok">Stok:</label>
                    <input type="number" class="form-control" id="stok" value="{{ old( 'stok','') }}" placeholder="Masukkan stok" name="stok">
                    @error('stok')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
            
            <div class="form-group">
                <label for="harga">Harga:</label>
                    <input type="number" class="form-control" id="harga" value="{{ old( 'harga','') }}" placeholder="Masukkan harga" name="harga">
                    @error('harga')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
            
            <!-- <div class="form-group">
                <label for="tipe">Tipe:</label>
                    <input type="text" class="form-control" id="tipe" value="{{ old( 'tipe','') }}" placeholder="Masukkan tipe" name="tipe">
                    @error('tipe')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div> -->
            <div class="form-group">
                <label for="tipe">Tipe:</label>
                @error('tipe')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
                    <select id="tipe" class="form-control" name="tipe">
                        <option selected disabled>Pilih Tipe Barang</option>
                        <option value="Makanan">Makanan</option>
                        <option value="Minuman">Minuman</option>
                        <option value="Peralatan Mandi">Peralatan Mandi</option>
                        <option value="Alat Tulis">Alat Tulis</option>
                        <option value="Parfum">Parfum</option>
                        <option value="Pengharum Ruangan">Pengharum Ruangan</option>
                     </select>
            </div>
            
        </div>
    <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Input</button>
            <a href="/dashboard" class="btn btn-secondary">Kembali</a>
        </div>
    </form>
</div>

@endsection