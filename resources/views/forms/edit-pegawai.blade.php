@extends('layouts.master')
@section('title','Daftar Pegawai')
@section('content')

<div class="card card-primary mt-5">
    <div class="card-header">
        <h3 class="card-title"><small>Ubah Pegawai</small></h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pegawai/{{$tampil->id}}/edit" method="POST">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
                <label for="nama">Nama:</label>
                    <input type="text" class="form-control" id="nama" value="{{ old( 'nama',$tampil->nama) }}" placeholder="Masukkan nama" name="nama">
                    @error('nama')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
            
            <div class="form-group">
                <label for="umur">Umur:</label>
                    <input type="number" class="form-control" id="umur" value="{{ old( 'umur',$tampil->umur) }}" placeholder="Masukkan umur" name="umur">
                    @error('umur')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
            
            <div class="form-group">
                <label for="alamat">Alamat:</label>
                    <input type="text" class="form-control" id="alamat" value="{{ old( 'alamat',$tampil->alamat) }}" placeholder="Masukkan alamat" name="alamat">
                    @error('alamat')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
            
            <div class="form-group">
                <label for="no_telpon">Nomor Telepon:</label>
                    <input type="text" class="form-control" id="no_telpon" value="{{ old( 'no_telpon',$tampil->no_telpon) }}" placeholder="Masukkan Nomor Telepon" name="no_telpon">
                    @error('no_telpon')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
            
            <!-- <div class="form-group">
                <label for="jk">Jenis Kelamin:</label>
                    <input type="text" class="form-control" id="jk" value="{{ old( 'jenis_kelamin',$tampil->jenis_kelamin) }}" placeholder="Masukkan jenis kelamin" name="jenis_kelamin">
                    @error('jenis_kelamin')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div> -->
            <div class="form-group">
                <label for="jk">Jenis Kelamin:</label>
                @error('jenis_kelamin')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
                    <select id="jk" class="form-control" name="jenis_kelamin">
                        <option selected value="{{$tampil->jenis_kelamin}}">{{$tampil->jenis_kelamin}}</option>
                        <option value="Pria">Pria</option>
                        <option value="Wanita">Wanita</option>
                     </select>
            </div>
            
            <div class="form-group">
                <label for="pend_terakhir">Pendidikan Terakhir:</label>
                    <input type="text" class="form-control" id="pend_terakhir" value="{{ old( 'pend_terakhir',$tampil->pend_terakhir) }}" placeholder="Masukkan pendidikan terakhir" name="pend_terakhir">
                    @error('pend_terakhir')
                        <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                    @enderror
            </div>
            
            
        </div>
    <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
            <a href="/pegawai" class="btn btn-secondary">Kembali</a>
        </div>
    </form>
</div>

@endsection