@extends('layouts.master')
@section('title','Data Penjualan')
@section('content')

<div class="card card-primary mt-5">
    <div class="card-header">
        <h3 class="card-title"><small>Edit Penjualan</small></h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/penjualan/{{$tampil->id}}/edit" method="POST">
        @csrf
        @method('put')
        <div class="card-body">
            <div class="form-group">
                <label for="nama_barang">Nama barang:</label>
                <input type="text" class="form-control" id="nama_barang" value="{{ old( 'nama_barang',$edit ?? ''->nama_barang) }}" placeholder="Masukkan nama barang" name="nama_barang">
                @error('nama_barang')
                <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                @enderror
            </div>

            <div class="form-group">
                <label for="jumlah_yang_terjual">Jumlah yang terjual:</label>
                <input type="number" class="form-control" id="jumlah_yang_terjual" value="{{ old( 'jumlah_yang_terjual',$edit ?? ''->jumlah_yang_terjual) }}" placeholder="Masukkan jumlah yang terjual" name="jumlah_yang_terjual">
                @error('jumlah_yang_terjual')
                <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                @enderror
            </div>

            <div class="form-group">
                <label for="harga_yang_terjual">harga yang terjual:</label>
                <input type="number" class="form-control" id="harga_yang_terjual" value="{{ old( 'harga_yang_terjual',$edit ?? ''->harga_yang_terjual) }}" placeholder="Masukkan harga yang terjual" name="harga_yang_terjual">
                @error('harga_yang_terjual')
                <div class="alert alert-danger mt-2 mb-3"><small>{{ $message }}</small></div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
            <a href="/dashboard" class="btn btn-secondary">Kembali</a>
        </div>
    </form>

</div>

@endsection