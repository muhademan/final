@extends('layouts.master')

@section('title','Data Barang di Gudang')
@section('content')

<!-- About Me Box -->
<div class="card card-info mt-5">
              <div class="card-header">
                <h3 class="card-title">Tentang {{$tampil->nama}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong>Harga</strong>

                <p class="text-muted">
                 Rp. {{ $tampil->harga}}
                </p>

                <hr>

                <strong>Stok</strong>

                <p class="text-muted">{{$tampil->stok}} Dus</p>

                <hr>

                <strong>Tipe</strong>

                <p class="text-muted">
                  {{$tampil->tipe}}
                </p>
              </div>
              <!-- /.card-body -->
            </div>
            <a href="/dashboard" class="btn btn-primary mt-1">Kembali</a>
            <!-- /.card -->

@endsection