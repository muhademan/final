@extends('layouts.master')

@section('title','Data Penjualan Barang')
@section('content')

<!-- About Me Box -->
<div class="card card-info mt-5">
    <div class="card-header">
        <h3 class="card-title">Nama Barang {{$tampil->nama_barang}}</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <strong>Jumlah Yang Terjual</strong>

        <p class="text-muted">
            {{ $tampil->jumlah_yang_terjual}}
        </p>

        <hr>

        <strong>Harga Yang Terjual</strong>

        <p class="text-muted">Rp. {{$tampil->harga_yang_terjual}} Dus</p>

        <hr>
    </div>
    <!-- /.card-body -->
</div>
<a href="/penjualan" class="btn btn-primary mt-1">Kembali</a>
<!-- /.card -->

@endsection