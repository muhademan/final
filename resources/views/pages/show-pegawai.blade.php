@extends('layouts.master')

@section('title','Data Pegawai')
@section('content')

<div class="card card-primary card-outline">
              <div class="card-body box-profile mt-2">

                <h3 class="profile-username text-center">{{ $tampil->nama}}</h3>

                <p class="text-muted text-center">{{$tampil->umur}} Tahun</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Nomor Telepon</b> <a class="float-right">{{$tampil->no_telpon}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Alamat</b> <a class="float-right">{{$tampil->alamat}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Jenis Kelamin</b> <a class="float-right">{{$tampil->jenis_kelamin}}</a>
                  </li>
                  
                  <li class="list-group-item">
                    <b>Pendidikan Terakhir</b> <a class="float-right">{{$tampil->pend_terakhir}}</a>
                  </li>
                </ul>

                <a href="/pegawai" class="btn btn-primary btn-block"><b>Kembali</b></a>
              </div>
              <!-- /.card-body -->
            </div>

@endsection