@extends('layouts.master')

@section('title','Daftar Pegawai')
@section('content')


<div class="card mt-3">
<div class="card-body">
    @if(session('success'))
        <div class="alert alert-success">{{session('success')}}</div>
    @endif
    <a href="/create-pegawai" class="btn btn-primary mb-3">Tambah Pegawai</a>
        <table id="example1" class="table table-bordered table-hover">
            <thead>
            <tr>
            <th>#</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Alamat</th>
            <th>No Telepon</th>
            <th>Jenis Kelamin</th>
            <th>Pendidikan Terakhir</th>
            <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @forelse($tampil as $key => $t)
            <tr>
            <td>{{$key + 1}}</td>
            <td>{{$t->nama}}</td>
            <td>{{$t->umur}}</td>
            <td>{{$t->alamat}}</td>
            <td>{{$t->no_telpon}}</td>
            <td>{{$t->jenis_kelamin}}</td>
            <td>{{$t->pend_terakhir}}</td>
            <td style="display:flex">
                <a href="/pegawai/{{$t->id}}" class="btn btn-info btn-sm">Show</a>
                <a href="/pegawai/{{$t->id}}/edit" class="btn btn-warning btn-sm ml-1">Edit</a>
                <form action="/pegawai/{{$t->id}}/delete" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger btn-sm ml-1" value="Delete">
                </form>
            </td>
            </tr>
            @empty
            <tr>
                <td colspan="8" class="text-center">No Data In Here</td>
            </tr>
            @endforelse
            </tbody>
        </table>
</div>
<!-- /.card-body -->
</div>
            <!-- /.card -->


@endsection

@push('script')

    <script src="{{ asset('/assets/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
        $(function () {
            $("#example1").DataTable();
        });
    </script>

@endpush