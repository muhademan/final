<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/dashboard', 'BarangController@index');
Route::get('/create-barang', 'BarangController@create');
Route::post('/barang-masuk', 'BarangController@store');
Route::get('/barang/{id}', 'BarangController@show');
Route::get('/barang/{id}/edit', 'BarangController@edit');
Route::put('/barang/{id}/edit', 'BarangController@update');
Route::delete('/barang/{id}/delete', 'BarangController@destroy');

Route::get('/pegawai', 'PegawaiController@index');
Route::get('/create-pegawai', 'PegawaiController@create');
Route::post('/pegawai-input', 'PegawaiController@store');
Route::get('/pegawai/{id}', 'PegawaiController@show');
Route::get('/pegawai/{id}/edit', 'PegawaiController@edit');
Route::put('/pegawai/{id}/edit', 'PegawaiController@update');
Route::delete('/pegawai/{id}/delete', 'PegawaiController@destroy');

Route::get('/penjualan', 'PenjualanbarangController@index');
Route::get('/create-penjualan', 'PenjualanbarangController@create');
Route::post('/penjualan-input', 'PenjualanbarangController@store');
Route::get('/penjualan/{id}', 'PenjualanbarangController@show');
Route::get('/penjualan/{id}/edit', 'PenjualanbarangController@edit');
Route::put('/penjualan/{id}/edit', 'PenjualanbarangController@update');
Route::delete('/penjualan/{id}/delete', 'PenjualanbarangController@destroy');
